import os
import sys
import shutil
import subprocess
import argparse


class OsPopen(object):

    def __init__(self, Popen=None):
        self.Popen = Popen if Popen else subprocess.Popen

    @property
    def path(self):
        return self

    @property
    def sep(self):
        return '/'

    def join(self, *args):
        path = args[0]
        for name in args[1:]:
            if path.endswith(self.sep):
                if name.startswith(self.sep):
                    path = path.rstrip(self.sep) + name
                else:
                    path += name
            else:
                if name.startswith(self.sep):
                    path += name
                else:
                    path += self.sep + name
        return path

    def dirname(self, path):
        if path.find(self.sep) == -1:
            return ''
        return self.sep.join(path.split(self.sep)[:-1])

    def basename(self, path):
        if path.find(self.sep) == -1:
            return path
        return path.split(self.sep)[-1]

    @property
    def environ(self):
        values = dict()
        p = self.Popen('export', shell=True,
            stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        stdout, _ = p.communicate()
        text = stdout.decode().strip()
        for line in text.split('\n'):
            if line.find('=') == -1:
                continue
            items = line.split('=')
            key = items[0].split(' ')[-1]
            value = '='.join(items[1:])
            if value.startswith("'") and value.endswith("'"):
                value = value[1:-1]
            if value.startswith('"') and value.endswith('"'):
                value = value[1:-1]
            values[key] = value
        return values

    def expanduser(self, path):
        if not path.startswith('~'):
            return path
        home = self.environ.get('HOME', self.getcwd())
        return home + path[1:]

    def realpath(self, path):
        if not path.startswith(self.sep):
            path = self.join(self.getcwd(), path)
        items = path.split(self.sep)
        path = self.sep
        for name in items:
            if name == '':
                continue
            if name == '..':
                path = self.dirname(path)
                if path == '':
                    path = self.sep
            else:
                path = self.join(path, name)
        return path

    def getcwd(self):
        p = self.Popen(['pwd'],
            stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        stdout, _ = p.communicate()
        path = stdout.decode().strip()
        assert path.startswith('/')
        return path

    def isdir(self, path):
        p = self.Popen(['test', '-d', path],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        p.communicate()
        return 0 == p.returncode

    def isfile(self, path):
        p = self.Popen(['test', '-f', path],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        p.communicate()
        return 0 == p.returncode

    def exists(self, path):
        return self.isfile(path) or self.isdir(path)

    def makedirs(self, path):
        p = self.Popen(['mkdir', '-p', path],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        p.communicate()
        assert self.exists(path)

    def cpu_count(self):
        p = self.Popen(['nproc'],
            stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        stdout, _ = p.communicate()
        cpus = stdout.decode().strip()
        cpus = int(cpus)
        assert cpus > 0
        return cpus

    def remove(self, path):
        if not self.exists(path):
            raise FileNotFoundError(path)
        p = self.Popen(['rm', path],
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        p.communicate()
        assert not self.exists(path)


class CpioGzPopen(object):

    def __init__(self, Popen=None):
        self.Popen = Popen if Popen else subprocess.Popen
        self.src = None
        self.dst = None

    def compress(self):
        os = OsPopen(self.Popen)
        src = self.src
        assert src
        dst = self.dst
        assert dst
        src = os.path.realpath(os.path.expanduser(src))
        dst = os.path.realpath(os.path.expanduser(dst))
        if os.path.exists(dst):
            os.remove(dst)
        assert os.path.exists(src)
        assert os.path.isdir(src)
        assert not os.path.exists(dst)
        cmd = 'find . -print0 | cpio --null --create --format=newc | gzip --best > {}'.format(dst)
        p = self.Popen(cmd, cwd=src, shell=True, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
        p.communicate()
        assert os.path.exists(dst)


# https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
class LinuxKernelPopen(object):

    def __init__(self, Popen=None):
        self.Popen = Popen if Popen else subprocess.Popen
        self.arch = None
        self.source_path = None
        self.build_path = None
        self.image_path = None

    def build(self, args=None):
        os = OsPopen(self.Popen)
        build_args = args if args else ['-j', '{}'.format(os.cpu_count())]
        arch = self.arch
        source_path = self.source_path
        build_path = self.build_path
        assert arch
        assert source_path
        assert build_path
        source_path = os.path.realpath(os.path.expanduser(source_path))
        build_path = os.path.realpath(os.path.expanduser(build_path))
        if not os.path.exists(build_path):
            os.makedirs(build_path)
        assert os.path.exists(build_path)
        args = ['make']
        args.append('ARCH={}'.format(arch))
        args.append('O={}'.format(build_path))
        args.extend(build_args)
        p = self.Popen(args, cwd=source_path)
        p.communicate()
        assert 0 == p.returncode


# https://git.busybox.net/busybox
class BusyboxPopen(object):

    def __init__(self, Popen=None):
        self.Popen = Popen if Popen else subprocess.Popen
        self.arch = None
        self.source_path = None
        self.build_path = None

    @property
    def install_path(self):
        os = OsPopen(self.Popen)
        assert self.build_path
        path = os.path.join(self.build_path, '_install')
        return os.path.realpath(os.path.expanduser(path))

    def build(self, args=None):
        os = OsPopen(self.Popen)
        build_args = args if args else ['-j', '{}'.format(os.cpu_count())]
        arch = self.arch
        source_path = self.source_path
        build_path = self.build_path
        assert arch
        assert source_path
        assert build_path
        source_path = os.path.realpath(os.path.expanduser(source_path))
        build_path = os.path.realpath(os.path.expanduser(build_path))
        if not os.path.exists(build_path):
            os.makedirs(build_path)
        assert os.path.exists(build_path)
        args = ['make']
        args.append('ARCH={}'.format(arch))
        args.append('O={}'.format(build_path))
        args.extend(build_args)
        p = self.Popen(args, cwd=source_path)
        p.communicate()
        assert 0 == p.returncode

    def install(self):
        self.build(['install'])


class QemuPopen(object):

    def __init__(self, Popen=None):
        self.Popen = Popen if Popen else subprocess.Popen
        self.arch = None
        self.nographic = True
        self.initrd = None
        self.kernel = None
        self.command_line = None

    def shell(self):
        os = OsPopen(self.Popen)
        arch = self.arch
        initrd = self.initrd
        kernel = self.kernel
        command_line = self.command_line
        assert arch
        args = ['qemu-system-{}'.format(arch)]
        if self.nographic:
            args.append('-nographic')
        if initrd:
            initrd = os.path.realpath(os.path.expanduser(initrd))
            args.extend(['-initrd', initrd])
        if kernel:
            kernel = os.path.realpath(os.path.expanduser(kernel))
            args.extend(['-kernel', kernel])
        if not command_line:
            command_line = 'console=ttyS0'
        args.extend(['-append', command_line])
        p = self.Popen(args)
        p.communicate()


class LocalXdb(object):

    @property
    def Popen(self):
        return self

    def __call__(self, args, *_args, **_kwargs):
        if not args or len(args) == 0:
            if os.name == 'posix':
                args = ['bash']
            else:
                raise NotImplementedError(os.name)
        return subprocess.Popen(args, *_args, **_kwargs)

    def build(self, args):
        pass

    def push(self, src, dst):
        return self.copy(src, dst)

    def pull(self, src, dst):
        return self.copy(src, dst)

    def copy(self, src, dst):
        if dst == '.':
            dst = os.path.join(os.getcwd(), '.')
        if dst.endswith(os.sep + '.'):
            dst = os.path.join(os.path.dirname(dst), os.path.basename(src))
        src = os.path.realpath(src)
        dst = os.path.realpath(dst)
        if src == dst:
            return
        if not os.path.exists(src):
            raise FileNotFoundError(src)
        if os.path.isfile(src):
            if os.path.exists(dst):
                os.remove(dst)
            if not os.path.exists(os.path.dirname(dst)):
                os.makedirs(os.path.dirname(dst))
            assert os.path.exists(os.path.dirname(dst))
            assert not os.path.exists(dst)
            shutil.copyfile(src, dst)
            assert os.path.exists(dst)
            return
        raise NotImplementedError('directory')


class QemuLinuxKernelXdb(object):

    def __init__(self, parent=None):
        self.parent = parent if parent else LocalXdb()
        self.busybox = BusyboxPopen(self.parent.Popen)
        self.kernel = LinuxKernelPopen(self.parent.Popen)
        self.initrd_path = None
        self.initrd_file = None

    def build(self, args):
        os = OsPopen(self.parent.Popen)
        initrd_path = self.initrd_path
        assert initrd_path
        initrd_path = os.path.realpath(os.path.expanduser(initrd_path))
        os.makedirs(initrd_path)
        busybox_built = False
        if not args:
            self.busybox.build()
            self.busybox.install()
            busybox_built = True
            self.kernel.build(args)
        elif args[0] == 'busybox':
            self.busybox.build(args[1:])
            self.busybox.install()
            busybox_built = True
        elif args[0] == 'kernel':
            self.kernel.build(args[1:])
        else:
            raise NotImplementedError(args[0])
        if busybox_built:
            src = self.busybox.install_path + '/*'
            dst = initrd_path + '/.'
            p = self.parent.Popen(' '.join(['rsync', '-avv', src, dst]), shell=True)
            p.communicate()
            assert 0 == p.returncode

    def pack(self):
        os = OsPopen(self.parent.Popen)
        initrd_path = self.initrd_path
        assert initrd_path
        initrd_path = os.path.realpath(os.path.expanduser(initrd_path))
        initrd_file = self.initrd_file
        assert initrd_file
        initrd_file = os.path.realpath(os.path.expanduser(initrd_file))
        if not os.path.exists(initrd_path):
            self.build(list())
        init_file = os.path.join(initrd_path, 'init')
        if not os.path.exists(init_file):
            raise Exception('generate and write init file')
        assert os.path.exists(initrd_path)
        cpio_gz = CpioGzPopen(self.parent.Popen)
        cpio_gz.src = initrd_path
        cpio_gz.dst = initrd_file
        cpio_gz.compress()

    def realinitrdpath(self, path):
        os = OsPopen(self.parent.Popen)
        initrd_path = self.initrd_path
        assert initrd_path
        initrd_path = os.path.realpath(os.path.expanduser(initrd_path))
        if not os.path.exists(initrd_path):
            self.build(list())
        while path.startswith('/') or path.startswith('.') or path.startswith('~'):
            path = path[1:]
        if path == '':
            path = '.'
        return os.path.join(initrd_path, path)

    @property
    def Popen(self):
        return self

    def __call__(self, args, *_args, **_kwargs):
        if not args or len(args) == 0:
            os = OsPopen(self.parent.Popen)
            initrd_file = self.initrd_file
            assert initrd_file
            initrd_file = os.path.realpath(os.path.expanduser(initrd_file))
            if not os.path.exists(initrd_file):
                self.pack()
            qemu = QemuPopen(self.parent.Popen)
            qemu.arch = self.kernel.arch
            qemu.initrd = initrd_file
            qemu.kernel = self.kernel.image_path
            qemu.shell()
            return
        raise NotImplementedError

    def push(self, src, dst):
        self.parent.push(src, self.realinitrdpath(dst))
        self.pack()

    def pull(self, src, dst):
        self.parent.push(self.realinitrdpath(src), dst)


def device(args):
    device = QemuLinuxKernelXdb(parent=LocalXdb())
    device.initrd_path = '~/initrd'
    device.initrd_file = '~/initrd.cpio.gz'
    device.busybox.arch = 'x86_64'
    device.busybox.source_path = '~/busybox'
    device.busybox.build_path = '~/busybox/build'
    device.kernel.arch = 'x86_64'
    device.kernel.source_path = '~/linux'
    device.kernel.build_path = '~/linux/build'
    device.kernel.image_path = '~/linux/build/arch/x86/boot/bzImage'
    return device


def build(args):
    args.device.build(args.args)


def pull(args):
    args.device.pull(args.src, args.dst)


def push(args):
    args.device.push(args.src, args.dst)


def shell(args):
    p = args.device.Popen(args.args)
    p.communicate()
    sys.exit(p.returncode)


def help(args):
    args.parser.print_help()


def main():
    parser = argparse.ArgumentParser(prog='eXtended Debug Bridge (XDB)')
    parser.set_defaults(func=help)
    parser.add_argument('-d', '--device', type=str, default=None)
    subparsers = parser.add_subparsers()
    parser_build = subparsers.add_parser('build')
    parser_build.set_defaults(func=build)
    parser_push = subparsers.add_parser('push')
    parser_push.add_argument('src')
    parser_push.add_argument('dst')
    parser_push.set_defaults(func=push)
    parser_push = subparsers.add_parser('pull')
    parser_push.add_argument('src')
    parser_push.add_argument('dst')
    parser_push.set_defaults(func=pull)
    parser_shell = subparsers.add_parser('shell')
    parser_shell.set_defaults(func=shell)
    args, argv = parser.parse_known_args()
    args.parser = parser
    args.args = argv
    args.device = device(args)
    args.func(args)


if __name__ == '__main__':
    main()
